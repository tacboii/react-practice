import React from 'react';
import RightSidebar from '../layouts/right-sidebar'
import Container from '../layouts/main-container'

class Homepage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      people: [],
      container: []
    }
  }
  componentDidMount() {
    Promise.all([
      fetch('http://www.mocky.io/v2/5d94ab202f00006b008ff8c5'),
      fetch('http://www.mocky.io/v2/5d94ac432f00006d008ff8cb'),
    ]).then(([res1,res2]) => {
      res1.json()
      res2.json()
      debugger
    })
  }

  render() {
    return (
      <div className="homepage">
        <div className="left-sidebar"></div>
        <Container/>
        <RightSidebar/>
      </div>
    )
  }
}

export default Homepage;
