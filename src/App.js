import React from 'react';
import './assets/styles/styles.scss';
import Homepage from './pages/homepage'
import Header from './layouts/header'
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

class App extends React.Component {

  componentDidMount() {
  }

  render() {
    return (
      <div>
        <Header/>
        <Switch>
          <Route exact path="/" component={Homepage}/>
        </Switch>
      </div>
    )
  }
}

export default App;
